.. _about-index:

.. toctree::
    :maxdepth: 2
    :hidden:

    license.rst

###############
About |pyretis|
###############

|pyretis| is a `Python <https://www.python.org>`_ library
for **rare event molecular simulations**
with emphasis on methods based on
:ref:`transition interface sampling <user-guide-tis-theory>`
and :ref:`replica exchange transition interface sampling <user-guide-retis-theory>`.
The papers describing the |pyretis| program
can be found here: `<https://doi.org/10.1002/jcc.24900>`_ (|pyretis| 1, 2017),
here: `<https://doi.org/10.1002/jcc.26112>`_ (|pyretis| 2, 2019).
and here: `<https://doi.org/10.1002/jcc.27319>`_ (|pyretis| 3, 2024).

The work on |pyretis| was initiated by `Titus van Erp <http://www.van-erp.org>`_ and is used in
the research and teaching activities in the
`theoretical chemistry <http://www.ntnu.edu/chemistry/research/theoretical-chemistry>`_
group at the `Norwegian University of Science and Technology <http://www.ntnu.edu/>`_.

|pyretis| is open source and is released
under a :ref:`GNU Lesser General Public license v2.1+ <pyretis-license>`.
If you are interested in contributing to the |pyretis| project,
please have a look
at the :ref:`developer guide <developer-guide-index>` and visit our
git repository `<https://gitlab.com/pyretis/pyretis>`_.

The current |pyretis| version is |version|.
For an overview of the official |pyretis| releases,
please visit our git repository: `<https://gitlab.com/pyretis/pyretis/-/releases>`_.

Since version 2.4, |pyretis| includes |pyvisa|, a program created to
facilitate post-processing and data analysis.


The |pyretis| team
------------------

**Head authors & project leaders:**

* `Titus van Erp <https://www.ntnu.edu/employees/titus.van.erp>`__

* `Enrico Riccardi <https://www.uis.no/nb/profile/enrico-riccardi>`_


**Contributors:**

* `An Ghysels <https://www.ugent.be/ea/ibitech/en/about-us/biommeda-staff-1/professor-an-ghysels.htm>`_

**Former Developers:**

* `Daniel Tianhou Zhang <https://www.linkedin.com/in/daniel-tianhou-zhang-857545192>`_

* `Wouter Vervust <https://www.ugent.be/ea/ibitech/en/about-us/biommeda-staff-1/ir-wouter-vervust.htm>`_

* `Ola Aarøen <https://www.linkedin.com/in/ola-aar%C3%B8en-64023678>`_

* `Sander Roet  <https://www.linkedin.com/in/sander-roet>`_

* `Anders Lervik <https://www.ntnu.edu/employees/anders.lervik>`_

* `Henrik Kiær <https://www.linkedin.com/in/henrikkiaer/?originalSubdomain=no>`_

* `Anastasia Maslechko <https://www.linkedin.com/in/anastasia-maslechko-87207b95/?originalSubdomain=ua>`_


**Acknowledgments:**

* Oda Dahlen

* Christopher Daub

* Mahmoud Moqadam

* César A. Urbina-Blanco 

* Jocelyne Vreede

* Magnus Heskestad Waage

* `Sudi Jawahery <https://www.linkedin.com/in/sudi-jawahery-78559b37>`_

* `Raffaela Cabriolu <https://www.linkedin.com/in/raffaela-cabriolu-41ba5488/?originalSubdomain=it>`_


|

To cite us:
-----------

When using |pyretis|, or one of our libraries, please cite us!

**Software papers:**

* `PyRETIS 3, 2024 <https://doi.org/10.1002/jcc.27319>`_

* `PyRETIS 2, 2019 <https://doi.org/10.1002/jcc.26112>`_

* `PyRETIS 1, 2017 <https://doi.org/10.1002/jcc.24900>`_


**Theory papers from our group:**

* `REPPTIS <https://doi.org/10.1016/j.bpj.2023.02.021>`_

* `Wire Fencing <https://doi.org/10.1063/5.0127249>`_

* `Stone Skipping & Web Throwing <https://doi.org/10.1021/acs.jpclett.7b01617>`_

* `RETIS <https://doi.org/10.1103/PhysRevLett.98.268301>`_

* `TIS <https://doi.org/10.1063%2F1.1562614>`_

* `Permeability calculation <https://doi.org/10.1103/PhysRevResearch.3.033068>`_
