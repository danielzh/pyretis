
.. image:: https://gitlab.com/pyretis/pyretis/badges/master/pipeline.svg
    :target: https://gitlab.com/pyretis/pyretis/commits/master

.. image:: https://gitlab.com/pyretis/pyretis/badges/master/coverage.svg
    :target: https://gitlab.com/pyretis/pyretis/commits/master

.. image:: https://app.codacy.com/project/badge/Grade/b276d2748d6448c8ac31e4657aa800b4
    :target: https://app.codacy.com/gl/pyretis/pyretis/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade

.. image:: https://badge.fury.io/py/pyretis.svg
    :target: https://badge.fury.io/py/pyretis

PyRETIS
=======

PyRETIS is a library for **rare event simulations**
with emphasis on methods based on transition interface sampling
replica exchange transition interface sampling.

PyRETIS is open source (see the COPYING file)
and can be interfaced with other simulation packages such as GROMACS or
CP2K.

The documentation is located at http://www.pyretis.org or it can
found in the docs directory of the source code.
