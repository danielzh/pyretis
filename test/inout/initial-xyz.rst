Particles
---------
position = {'input_file': 'configtag.xyz'}
ptype = [0, 0, 0, 1, 1]
mass = {'Ar': 1.,
        'Kr': 2.09767698,
        'Kr2': 2.09767698}

System
------
units = lj
